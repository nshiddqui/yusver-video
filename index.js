const fs = require('fs');
const request = require("request");
const axios = require('axios');
const FormData = require('form-data');
const tts = require('./voice-rss-tts');
var cheerio = require('cheerio');

const express = require('express');
const app = express();

const PORT = 3000;

app.get('/', (req, srver_response) => {
    srver_response.setTimeout(0);
    console.log('Process Start');
    axios.get('https://yuserver.in/home/getLatestBlog').then(function(response) {
        console.log('Get Json File from Yuserver Blog');
        blog_data = response.data;
        var id = response.data['blog_content'].id;
        axios.get('https://yuserver.in/home/uploadSuccess/' + id, function(response) {});
        fs.writeFile('blog_data.json', JSON.stringify(blog_data), function() {});
        console.log('Processing TTS');
        tts.speech({
            key: '1180a0c69d9a427996beb5be03715b84',
            hl: 'en-us',
            src: blog_data['blog_content'].content,
            r: -1,
            v: 'Mary',
            c: 'MP3',
            f: 'ulaw_44khz_stereo',
            ssml: false,
            b64: false,
            callback: async function(error, content) {
                console.log('Complete TTS');
                console.log('Processing Image upload');
                request('https://yuserver.in/img/' + blog_data['blog_content'].image)
                    .pipe(fs.createWriteStream('image.png'))
                    .on('close', function() {
                        console.log('Processing MP3 uploading');
                        fs.createWriteStream('sample-input.mp3').write(content, function() {
                            console.log('Complete MP3 uploading');
                            var data = new FormData();
                            data.append('language_prefixe', ' ');
                            data.append('audio[]', fs.createReadStream('sample-input.mp3'));
                            data.append('picture[]', fs.createReadStream('image.png'));
                            data.append('image_background_selected', ' ');
                            data.append('Fontname_parameter', ' Lato');
                            data.append('Fontsize_parameter', ' 26');
                            data.append('position', ' 030');
                            data.append('Colour_parameter', ' #ffffff');
                            data.append('outline_shadow', ' 2');
                            data.append('Background_Box', ' 0');
                            data.append('lyric_0', ' ');
                            data.append('Text_Effect_0', ' 1');
                            data.append('logo[]', ' ');
                            data.append('logo_position', ' overlay=main_w-overlay_w-10:main_h-overlay_h-10');
                            data.append('logo_width', ' 100');
                            data.append('pourcentage_folder', ' 1607597756109479222');
                            data.append('effect_selected', ' effects_gif/39.gif');

                            var config = {
                                method: 'post',
                                url: 'https://voice2v.com/processor.php',
                                headers: {
                                    ...data.getHeaders()
                                },
                                data: data
                            };
                            console.log('Processing Video converting');
                            axios(config)
                                .then(async function(response) {
                                    console.log('Completing Video converting');
                                    var $ = cheerio.load(response.data);
                                    var linkHrefs = $('source[type="video/mp4"]').attr('src');
                                    console.log('Prosessing Video Download');
                                    request(linkHrefs)
                                        .pipe(fs.createWriteStream('output-video.mp4'))
                                        .on('close', function() {
                                            // axios.get('https://python.shelty.in/', function(response) {});
                                            srver_response.send(linkHrefs);
                                        });
                                });
                        });
                    });
            }
        });
    });
});

app.listen(PORT, () => {
    console.log(`Server running at: http://localhost:${PORT}/`);
});